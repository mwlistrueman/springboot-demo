package com.sitech.mecd.springbootdemo.service.impl;

import com.sitech.mecd.springbootdemo.service.MyService;
import org.springframework.stereotype.Component;

/**
 * @author mawl
 * @date 2021/7/30 11:46
 */
@Component
public class MyServiceImpl implements MyService {
}
