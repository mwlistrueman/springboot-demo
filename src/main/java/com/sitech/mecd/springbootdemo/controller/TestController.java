package com.sitech.mecd.springbootdemo.controller;

import com.sitech.mecd.springbootdemo.entity.TestEntity;
import com.sitech.mecd.springbootdemo.service.MyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author mawl
 * @date 2021/6/30 13:40
 */
@RequestMapping("test")
@RestController
public class TestController {

    @Resource
    private MyService myService;

    @GetMapping
    public TestEntity test() {
        TestEntity testEntity = new TestEntity();
        testEntity.setName("good");
        return testEntity;
    }
}