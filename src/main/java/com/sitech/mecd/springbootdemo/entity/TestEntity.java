package com.sitech.mecd.springbootdemo.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author mawl
 * @date 2021/6/30 13:41
 */
@Data
@Component
public class TestEntity {
    private String name;
}
